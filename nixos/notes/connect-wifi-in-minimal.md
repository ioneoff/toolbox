```bash
$ wpa_supplicant -B -i interface -c <(wpa_passphrase 'SSID' 'key')
```

(See more)[https://nixpkgs-manual-sphinx-markedown-example.netlify.app/installation/installing.xml.html#networking-in-the-installer]
