{ lib, pkgs, config, ... }:

with lib;

let
  cfg = config.services.wlsunset;
in {
  options.services.wlsunset = {
    enable = mkEnableOption "wlsunset service";

    latitude = mkOption { type = types.float; };
    longitude = mkOption { type = types.float; };
  };

  config = mkIf cfg.enable {
    systemd.user.services.wlsunset = {
      enable = true;

      wantedBy = [ "default.target" ];
      serviceConfig.ExecStart = "${pkgs.wlsunset}/bin/wlsunset -l ${toString cfg.latitude} -L ${toString cfg.longitude}";
      serviceConfig.RestartSec = 3;
      serviceConfig.Restart = "always";
    };
  };
}
