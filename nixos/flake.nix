{
  description = "NixOS Configurations";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/b47fd6fa00c6afca88b8ee46cfdb00e104f50bca"; # branch: nixos-24.11 Dec 19, 2024
    # nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/80b0fdf483c5d1cb75aaad909bd390d48673857f"; # branch: release-24.11, Dec 16, 2024
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs:
    let
      inherit (self) outputs;
      forAllSystems = nixpkgs.lib.genAttrs [
        "aarch64-linux"
        "i686-linux"
        "x86_64-linux"
        "aarch64-darwin"
        "x86_64-darwin"
      ];
    in
    rec {
      # Your custom packages
      # Acessible through 'nix build', 'nix shell', etc
      packages = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in import ./pkgs { inherit pkgs; }
      );
      # Devshell for bootstrapping
      # Acessible through 'nix develop' or 'nix-shell' (legacy)
      devShells = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in import ./shell.nix { inherit pkgs; }
      );

      # Your custom packages and modifications, exported as overlays
      overlays = import ./overlays { inherit inputs; };

      # Reusable nixos modules you might want to export
      # These are usually stuff you would upstream into nixpkgs
      nixosModules = import ./modules/nixos;

      # Reusable home-manager modules you might want to export
      # These are usually stuff you would upstream into home-manager
      homeManagerModules = import ./modules/home-manager;

      # NixOS configuration entrypoint
      # Available through 'nixos-rebuild [build|switch|...] --flake .#your-hostname'
      nixosConfigurations = {
        # Laptop
        fw-minimal = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            inputs.nixos-hardware.nixosModules.framework
            ./hosts/fw/minimal/nixos
          ];
        };
        fw-i3 = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            inputs.nixos-hardware.nixosModules.framework
            ./hosts/fw/i3/nixos
          ];
        };
        fw-sway = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            inputs.nixos-hardware.nixosModules.framework-11th-gen-intel
            ./hosts/fw/sway/nixos
          ];
        };

        # VPS
        din = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            ./hosts/din/nixos
          ];
        };

        # Local server
        asus-minimal = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            ./hosts/asus/minimal/nixos
          ];
        };

        # Workstation
        workstation = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            inputs.nixos-hardware.nixosModules.lenovo-thinkpad-t14
            ./hosts/workstation/nixos
          ];
        };
      };

      # Standalone home-manager configuration entrypoint
      # Available through 'home-manager [build|switch|...] --flake .#your-username@your-hostname'
      homeConfigurations = {
        # Laptop
        "valery@fw-minimal" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs { system = "x86_64-linux"; config.allowUnfree = true; };
          extraSpecialArgs = { inherit inputs outputs; };
          modules = [
            ./hosts/fw/minimal/home
          ];
        };
        "valery@fw-3" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs { system = "x86_64-linux"; config.allowUnfree = true; };
          extraSpecialArgs = { inherit inputs outputs; };
          modules = [
            ./hosts/fw/i3/home
          ];
        };
      };
    };
}
