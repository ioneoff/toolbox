{ config, pkgs, ... }:

{
  programs.git = {
    enable = true;
    userName = "Valery Ivanov";
    userEmail = "ivalery111@gmail.com";
    delta = {
      enable = true;
      options = {
        line-numbers = "true";
        side-by-side = "true";
        decorations = "true";
      };
    };
    extraConfig = {
      core = {
        editor = "vim";
        whitespace = [
          "blank-at-eol"
          "blank-at-eof"
          "space-before-tab"
        ];
      };
      interactive = {};
      merge = {
        conflictstyle = "diff3";
      };
      diff = {
        colorMoved = "default";
      };
    };
  };

  home.packages = [
    pkgs.tk
  ];
}
