{ config, lib, pkgs, ... }:

{
  programs.rofi = {
    enable = true;
    theme = ./nord.rasi;
  };

}
