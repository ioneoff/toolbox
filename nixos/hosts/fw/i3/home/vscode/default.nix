{ config, pkgs, ... }:

{
  programs.vscode = {
    enable = true;
    extensions = [
      pkgs.vscode-extensions.twxs.cmake
      pkgs.vscode-extensions.llvm-vs-code-extensions.vscode-clangd
      pkgs.vscode-extensions.bbenoist.nix
      pkgs.vscode-extensions.eamodio.gitlens
    ];
  };
}

