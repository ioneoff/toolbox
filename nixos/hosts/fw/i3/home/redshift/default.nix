{ config, pkgs, ... }:

{
  services.redshift = {
    enable = true;
    latitude = "55.75222";
    longitude = "37.61556";
  };
}
