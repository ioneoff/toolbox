{ config, lib, pkgs, ... }:
let
  mod = "Mod4";
in
{
  imports = [
    ./i3status
  ];

  xsession.windowManager.i3 = {
    enable = true;
    config = null;
  };
  home.packages = [
    pkgs.brightnessctl
    pkgs.betterlockscreen
    pkgs.xidlehook
    pkgs.xautolock
    pkgs.xss-lock
  ];
  xdg.configFile."i3/config".source = lib.mkForce ./config;
}
