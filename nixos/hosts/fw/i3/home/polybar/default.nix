{ config, pkgs, ... }:

let
  customPolybar = pkgs.polybar.override {
    i3Support = true;
    pulseSupport = true;
  };

  colors  = builtins.readFile ./colors.ini;
  bars    = builtins.readFile ./bars.ini;
  modules = builtins.readFile ./modules.ini;
in
{
  services.polybar = {
    enable = true;
    config = ./config.ini;
    extraConfig = colors + bars + modules;
    package = customPolybar;
    script = '''';
  };

  xdg.configFile."polybar/launch.sh" = {
    source = ./scripts/launch.sh;
    executable = true;
  };
}
