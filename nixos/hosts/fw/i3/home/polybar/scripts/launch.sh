#!/usr/bin/env bash

# Terminate already running bar instances
pkill -9 polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    echo "==================================="
    MONITOR=$m polybar --reload top &
  done
else
  echo "+++++++++++++++++++++++++++++++++++++"
  polybar --reload top &
fi
