{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    vlc
    qbittorrent
    flameshot
    obsidian
    wireguard-tools
    telegram-desktop

    # Development
    clang-tools_16
    gcc12
  ];
}
