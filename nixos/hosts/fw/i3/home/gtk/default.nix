{ config, pkgs, ... }:

{
  gtk = {
    enable = true;
    theme = {
      name = "Nordic";
      package = pkgs.nordic;
    };
  };
  home.packages = [ pkgs.dconf ];
}
