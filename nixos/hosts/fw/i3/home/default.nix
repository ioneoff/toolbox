{ config, ... }:

{
  imports = [
    ./gtk
    ./kitty
    ./rofi
    ./starship
    ./zsh
    ./vscode
    ./wm/i3
    ./dunst
    ./feh
    ./git
    ./picom
    ./fzf
    ./zathura
    ./redshift
    ./direnv
    ./wine
    ./packages.nix
  ];

  home = {
    username = "v";
    homeDirectory = "/home/v";
  };

  home.stateVersion = "23.05";
}
