{ config, pkgs, ...}:

{
  imports = [
    ./firewall
  ];
  networking = {
    networkmanager = {
      enable = true;
    };
    hostName = "FW";
    nameservers = [
      "8.8.8.8"
      "8.8.4.4"
    ];
  };

  programs.nm-applet = {
    enable = true;
    indicator = true;
  };
}
