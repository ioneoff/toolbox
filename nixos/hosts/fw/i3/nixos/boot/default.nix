{ config, pkgs, ... }:

{
  boot = {
    loader = {
        systemd-boot.enable = true;
        efi.canTouchEfiVariables = true;
    };
    kernelParams = [
      "mem_sleep_default=deep"
      "nvme.noacpi=1" 
    ];
    supportedFilesystems = [ "ntfs" ];
  };
}
