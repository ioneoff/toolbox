{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./bluetooth
  ];

  services.fwupd = {
    enable = true;
  };

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = [
      pkgs.mesa_drivers
      pkgs.intel-compute-runtime
      pkgs.intel-media-driver
    ];
  };

  hardware.cpu = {
    intel.updateMicrocode = true;
  };
}
