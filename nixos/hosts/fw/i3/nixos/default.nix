{ config, pkgs, ... }:

{
  imports = [
    ./nix.nix
    ./boot
    ./font
    ./hardware
    ./locale
    ./networking
    ./services
    ./sound
    ./time
    ./users
    ./virtualisation
    ./wm/i3
    ./security/polkit
    ./keychain
    ./autorandr
    ./file_managers/thunar
  ];

  nixpkgs.config.allowUnfree = true;

  environment = {
    systemPackages = with pkgs; [
      vim
      git
      wget
    ];
    pathsToLink = [ "/libexec" ];
  };

  system.stateVersion = "23.05";
}
