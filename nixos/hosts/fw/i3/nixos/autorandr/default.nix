{ config, pkgs, ... }:

{
  services.autorandr = {
    enable = true;
  };
}
