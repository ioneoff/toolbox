{ config, pkgs, ... }:

{
  programs.zsh.enable = true;
  users.users.v = {
    isNormalUser = true;
    shell = pkgs.zsh;
    description = "v";
    extraGroups = [ "wheel" "docker" "libvirtd" "networkmanager" ];
    packages = with pkgs; [
      firefox
      neofetch
      pulseaudio
    ];
  };
}
