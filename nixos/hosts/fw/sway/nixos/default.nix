{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: 
let
  dbus-sway-environment= pkgs.writeTextFile {
    name = "dbus-sway-environment";
    destination = "/bin/dbus-sway-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop  pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Nordic'
    '';
  };

  curaAppImage = pkgs.callPackage ../../../../modules/nixos/cura.nix {};
in
{
  imports = [
    inputs.home-manager.nixosModules.home-manager
    ./hardware-configuration.nix
    ./bluetooth
    ./../../../common/nixos/fonts
    ./virtualisation

    ../../../../modules/nixos/wlsunset.nix

  ];

  home-manager = {
    extraSpecialArgs = { inherit inputs outputs; };
    users = {
      v = import ../home/home.nix;
    };
  };

  nixpkgs = {
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages
    ];
    # Configure your nixpkgs instance
    config = {
      allowUnfree = true;
    };
  };

  nix.settings = {
    experimental-features = "nix-command flakes";
    auto-optimise-store = true;
  };

  security.polkit.enable = true;

  networking.hostName = "FW";
  networking.networkmanager.enable = true;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.useOSProber = true;

  time.timeZone = "Europe/Moscow";

  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  programs.zsh.enable = true;
  users.users = {
    v = {
      initialHashedPassword = "$y$j9T$YmhoB6iXFL6uDQqZ0OUxz1$/gvttAxd7dhQBtIaJ.kPtLkp6OrYrWl3CqA2vmQF.43";
      isNormalUser = true;
      shell = pkgs.zsh;
      extraGroups = ["wheel" "video" "networkmanager" "audio" ];
      packages = with pkgs; [
        inputs.home-manager.packages.${pkgs.system}.default
        firefox
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    pulseaudio

    alacritty
    dbus-sway-environment
    configure-gtk
    wayland
    xdg-utils
    glib
    nordic
    gnome.adwaita-icon-theme
    swaylock
    swayidle
    grim
    slurp
    wl-clipboard
    bemenu
    wofi
    wdisplays

    wineWowPackages.stable
    winetricks
    wineWowPackages.waylandFull

    # Enable mans
    man-pages
    man-pages-posix

    curaAppImage

    openvpn

    freecad-wayland
  ];

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
    alsa.support32Bit = true;
    jack.enable = true;
  };

  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
  
    extraSessionCommands = ''
      NIXOS_OZONE_WL=1
    '';
  };

  services.greetd = {
    enable = true;
    settings = {
     default_session.command = ''
      ${pkgs.greetd.tuigreet}/bin/tuigreet \
        --time \
        --asterisks \
        --user-menu \
        --cmd sway
    '';
    };
  };
  environment.etc."greetd/environments".text = ''
    sway
  '';

  programs.light = {
    enable = true;
  };

  programs.thunar = {
    enable = true;
    plugins = with pkgs.xfce; [
      thunar-volman
    ];
  };

  services.gvfs.enable = true;

  services.fwupd.enable = true;

  services.wlsunset = {
    enable = true;

    latitude = 55.751244;
    longitude = 37.618423;
  };

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  system.stateVersion = "23.11";
}
