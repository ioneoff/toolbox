{ config, pkgs, ... }:

{
  imports = [
    ./docker
    ./qemu
  ];
}
