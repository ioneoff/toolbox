{ config, pkgs, ... }:

{
  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
  };

  users.users.v = {
    extraGroups = [ "docker" ];
  };
}
