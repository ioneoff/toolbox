{ config, pkgs, ... }:

{
  programs.virt-manager.enable = true;

  services.qemuGuest.enable = true;

  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.ovmf.enable = true;
  virtualisation.libvirtd.qemu.runAsRoot = true;
  virtualisation.libvirtd.onBoot = "ignore";
  virtualisation.libvirtd.onShutdown = "shutdown";

  users.users.v.extraGroups = [ "libvirtd" ];
}
