{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./sway
    ./i3status
    ./kitty
    ./vscode
    ./zsh
    ./mako
    ./wlsunset
    ./tmux
    ./../../../common/home/git
    ./../../../common/home/zathura
  ];

  nixpkgs = {
    overlays = [
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages
    ];

    config = {
      allowUnfree = true;
      allowUnfreePredicate = _: true;
    };
  };

  home = {
    username = "v";
    homeDirectory = "/home/v";
  };

  home.packages = with pkgs; [
    neofetch
    devbox
    telegram-desktop
    wireguard-tools
    qbittorrent
    cscope
    ctags
    gparted
    direnv
    vlc
    obsidian
    calibre
  ];

  programs.home-manager.enable = true;
  programs.git.enable = true;

  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.11";
}
