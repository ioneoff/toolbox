{ config, lib, pkgs, ... }:

{
  services.mako = {
    enable = true;

    defaultTimeout = 100;
  };

  home.packages = with pkgs; [
    libnotify
  ];
}