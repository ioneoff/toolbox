{ config, ... }:

{
  programs.kitty = {
    enable = true;
    shellIntegration = {
      enableBashIntegration = true;
      enableFishIntegration = true;
      enableZshIntegration = true;
    };
    theme = "Nord";
    settings = {
      enable_audio_bell = false;
      confirm_os_window_close = 0;
      font_family = "Fira Code";
      font_size = 12;
      background_opacity = "0.98";
    };
  };
}
