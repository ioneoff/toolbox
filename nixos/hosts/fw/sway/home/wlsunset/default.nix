{ config, ... }:

{
  services.wlsunset = {
    enable = true;
    latitude = "55.7558";
    longitude = "37.6173";
  };
}
