{ config, pkgs, ... }:

{
  programs.tmux = {
    enable = true;

    baseIndex = 1;
    clock24 = true;
    mouse = true;
    terminal = "screen-256color";
    prefix = "C-a";
    disableConfirmationPrompt = true;

    plugins = [
      pkgs.tmuxPlugins.nord
      pkgs.tmuxPlugins.cpu
    ];

    extraConfig = ''
    # split panes using | and -
    bind | split-window -h
    bind - split-window -v
    unbind '"'
    unbind %

    # resize tmux with Ctrl-Shift and arrows
    # bind-key -n C-S-Up resize-pane -U 5
    # bind-key -n C-S-Down resize-pane -D 5
    # bind-key -n C-S-Left resize-pane -L 10
    # bind-key -n C-S-Right resize-pane -R 10

    # inspired from http://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/
    # switch panes using Alt-arrow without prefix
    bind -n M-Left select-pane -L
    bind -n M-Right select-pane -R
    bind -n M-Up select-pane -U
    bind -n M-Down select-pane -D
    '';
  };
}
