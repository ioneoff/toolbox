{ config, ... }:

{
  imports = [
    ./zsh
    ./packages.nix
    ../../common/home/zellij
  ];

  home = {
    username = "valery";
    homeDirectory = "/home/valery";
  };

  home.stateVersion = "23.05";
}
