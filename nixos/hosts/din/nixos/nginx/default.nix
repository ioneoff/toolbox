{ config, pkgs, lib, ...}:

{
  services.nginx = {
    enable = true;

    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;

    virtualHosts."ioneoff.xyz" = {
      enableACME = true;
      forceSSL = true;
    };

    virtualHosts."blog.ioneoff.xyz" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        root = "/var/www/ioneoff.xyz";
      };
    };

    virtualHosts."git.ioneoff.xyz" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://10.0.0.2:3000/";
        extraConfig = ''
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
        '';
      };
    };
  };

  security.acme.acceptTerms = true;
  security.acme.certs = {
    "ioneoff.xyz".email = "ivalery111@gmail.com";
    "blog.ioneoff.xyz".email = "ivalery111@gmail.com";
    "git.ioneoff.xyz".email = "ivalery111@gmail.com";
  };
}
