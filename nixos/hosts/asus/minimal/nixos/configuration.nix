# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "ASUS"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Moscow";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.v = {
    isNormalUser = true;
    description = "v";
    extraGroups = [ "networkmanager" "wheel" ];
    hashedPassword = "$y$j9T$SrPO4cRCIPOpqy/LTS3Xw.$7Zkzn0XknE1ZZzY.UnBl9KP4KmdbIbM3RrX6zACSEI.";
    openssh.authorizedKeys.keys = [
	    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMv1i9NSpsd1SjO2MuXFrddaarlLUvq4LhAwUjLVbvD+ valery@asus"
	  ];
    packages = with pkgs; [
      wireguard-tools
      zellij
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;

    settings = {
      PermitRootLogin = "no";
    };
  };

  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [
    80
    443
    53 # Wireguard
    51820 # Wireguard
    3000 # Forgejo
  ];
  networking.firewall.allowedUDPPorts = [
    53 # Wireguard
    51820 # Wireguard
  ];

  #
  # [ Wireguard setup ]
  #

  networking.wg-quick.interfaces = {
    wg0 = {
      address = [ "10.0.0.2/24" "fdc9:281f:04d7:9ee9::2/64" ];
      dns = [ "10.0.0.1" "fdc9:281f:04d7:9ee9::1" ];
      privateKeyFile = "/root/wg/keys/asus.private";

      peers = [
        {
          publicKey = "SKu/jGe6PFXtU4xA/2AEK1RjhkdcL1uc5lQypO7+c2E=";
          allowedIPs = [ "10.0.0.0/24" "fdc9:281f:04d7:9ee9::2/64" ];
          endpoint = "109.107.171.88:51820";
          persistentKeepalive = 25;
        }
      ];
    };
  };

  #
  # [ End of Wireguard setup ]
  #

  users.users.forgejo = {
    home = "/var/lib/forgejo";
    useDefaultShell = true;
    group = "forgejo";
    isSystemUser = true;
  };

  services.forgejo = {
    enable = true;

    user = "forgejo";
    group = "forgejo";

    stateDir = "/var/lib/forgejo";

    settings = {
      service = {
        DISABLE_REGISTRATION = true;
      };
      server = {
        ROOT_URL = "https://ioneoff.xyz/";
        LANDING_PAGE = "explore";
      };
      actions = {
        ENABLED = true;
      };
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
