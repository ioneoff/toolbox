{ config, pkgs, ... }:

{
  fonts.packages = with pkgs; [
    corefonts
    font-awesome
    noto-fonts
    (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
  ];
  fonts.fontconfig = {
    defaultFonts = {
      monospace = [
        "Fira Code"
        "FiraCode Nerd Font"
        "DejaVu Sans Mono"
        "Noto Mono"
      ];
      sansSerif = [
        "Fira Sans"
        "DejaVu Sans"
        "Noto Mono"
      ];
      serif = [
        "Noto Serif"
      ];
    };
  };
}
