{ config, pkgs, ... }:

{
  programs.neovim = {
    enable = true;

    defaultEditor = true;

    extraConfig = ''
      set number
      syntax on
      set shiftwidth=4
      set smarttab
      set clipboard+=unnamedplus
    '';
  };
}
