{ config, pkgs, ... }:

{
  programs.i3status = {
    enable = true;
    enableDefault = false;
    general = {
      colors = true;
      color_good = "#36E592";
      color_degraded = "#CE9726";
      color_bad = "#CE4B4F";
      color_separator = "#B3BEFF";
      interval = 1;
    };
    modules = {
      "time" = {
        position = 6;
        settings = {
          format = "[ %Y-%m-%d %H:%M:%S ]";
        };
      };
      "volume master" = {
        position = 5;
        settings = {
          format = "[ 󰖀 %volume ]";
          format_muted = "[ 󰝟 ]";
          device = "pulse";
        };
      };
      "battery all" = {
        position = 4;
        settings = {
          format = "[ %status %percentage ]";
          status_chr = "󰂄";
          status_bat = "󰁹";
        };
      };
      "cpu_usage" = {
        position = 3;
        settings = {
          format = "[   %usage ]";
        };
      };
      "memory" = {
        position = 2;
        settings = {
          format = "[  %used ]";
        };
      };
      "disk /home" = {
        position = 2;
        settings = {
          format = "%free ]";
        };
      };
      "disk /" = {
        position = 2;
        settings = {
          format = "[ %free";
        };
      };
      "wireless wlp0s20f3" = {
        position = 1;
        settings = {
          format_up   = "[ %essid ]";
          format_down = "[ NO WIFI ]";
        };
      };
      "wireless tun0" = {
        position = 0;
        settings = {
          format_up   = "[ YVPN ]";
          format_down = "[ YVPN ]";
        };
      };
    };
  };
}
