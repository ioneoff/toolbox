{ config, pkgs, ... }:

{
  programs.git.enable = true;
  programs.git.userName = "Valery Ivanov";
  programs.git.delta.enable = true;
  programs.git.delta.options = {
    line-numbers = "true";
    side-by-side = "true";
    decorations = "true";
  };
  programs.git.extraConfig.core.editor = "nvim";
  programs.git.extraConfig.core.whitespace = [
    "blank-at-eol"
    "blank-at-eof"
    "space-before-tab"
  ];
  programs.git.extraConfig.interactive = {};
  programs.git.extraConfig.merge.conflictstyle = "diff3"; 
  programs.git.extraConfig.diff.colorMoved = "default";

  programs.git.package = pkgs.gitFull;

  home.packages = [
    pkgs.tig
  ];
}
