{ config, pkgs, ... }:

{
  programs.direnv.enable = true;
  programs.direnv.enableBashIntegration = true;
  programs.direnv.enableFishIntegration = true;
  programs.direnv.enableZshIntegration = true;
}
