{ config, pkgs, ... }:

{
  programs.vscode = {
    enable = true;
    extensions = [
      pkgs.vscode-extensions.twxs.cmake
      pkgs.vscode-extensions.llvm-vs-code-extensions.vscode-clangd
      pkgs.vscode-extensions.bbenoist.nix
      pkgs.vscode-extensions.eamodio.gitlens
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
      {
        name = "fzf-quick-open";
        publisher = "rlivings39";
        version = "0.5.1";
        sha256 = "xGcBl3mmyy+Zsn9OncDDbJViMxEgvsRjkzy89NPJpS8=";
      }
    ];
  };

  home.packages = [
    pkgs.fzf
    pkgs.ripgrep
  ];
}
