{ config, inputs, outputs, pkgs, ... }:

{
  imports = [
    ./sway
    ./i3status
    ./zsh
    ./vscode
    ./kitty
    ./git
    ./direnv
    ./neovim

    ./../../common/home/zellij
  ];

  nixpkgs.overlays = [
    outputs.overlays.additions
    outputs.overlays.modifications
    outputs.overlays.unstable-packages
  ];

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowUnfreePredicate = _: true;

  home.username = "v";
  home.homeDirectory = "/home/v";

  home.packages = with pkgs; [
    neofetch
    wireguard-tools
    qbittorrent
    cscope
    ctags
    vlc
    flameshot

    gnumake
  ];

  home.stateVersion = "23.11";
}
