filetype off

" Leader & Shell
let mapleader=','
set shell=/run/current-system/sw/bin/zsh

" Better Unix support
set viewoptions=folds,options,cursor,unix,slash
set encoding=utf-8

" True color support
set termguicolors

" Other options
syntax on

" Clear search highlighting
nnoremap <C-z> :nohlsearch<CR>

" General editor options
set hidden                  " Hide files when leaving them.
set number                  " Show line numbers.
set numberwidth=1           " Minimum line number column width.
set cmdheight=2             " Number of screen lines to use for the commandline.
set textwidth=0             " Lines length limit (0 if no limit).
set formatoptions=jtcrq     " Sensible default line auto cutting and formatting.
set linebreak               " Don't cut lines in the middle of a word .
set showmatch               " Shows matching parenthesis.
set matchtime=2             " Time during which the matching parenthesis is shown.
set listchars=tab:▸\ ,eol:¬ " Invisible characters representation when :set list.
set clipboard=unnamedplus   " Copy/Paste to/from clipboard

" Disable the annoying and useless ex-mode
nnoremap Q <Nop>
nnoremap gQ <Nop>

"
" Telescope settings
"
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"
" nvim-tree
"
nnoremap <leader>f <cmd>NvimTreeToggle<cr>
