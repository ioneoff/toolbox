{ config, pkgs, lib, ... }:

{
  programs.neovim = {
    enable = true;

    defaultEditor = true;

    extraConfig = lib.fileContents ./init.vim;
    extraLuaConfig = lib.fileContents ./init.lua;

    plugins = with pkgs; [
      vimPlugins.vim-airline
      vimPlugins.telescope-nvim
      vimPlugins.nvim-tree-lua
    ];

    extraPackages = [
      pkgs.fd
    ];
  };
}
