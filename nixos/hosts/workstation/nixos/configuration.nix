{ inputs, outputs, config, pkgs, ... }:

{
  imports =
  [
    inputs.home-manager.nixosModules.home-manager
    ./hardware-configuration.nix

    ./../../common/nixos/fonts

    ./boot
    ./networking
    ./time
    ./internationalization
    ./wayland/sway
    ./light
    ./thunar
    ./printing
    ./sound
    ./touchpad
    ./users
    ./nix
    ./ssh
    ./openvpn
    ./home-manager
    ./docker
    ./qemu
    ./system_packages
    ./gpu
    ./bluetooth
    ./dev

    ../../../modules/nixos/wlsunset.nix
  ];

  services.wlsunset = {
    enable = true;

    latitude = 55.751244;
    longitude = 37.618423;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
