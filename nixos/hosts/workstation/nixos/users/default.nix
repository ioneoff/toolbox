{ config, pkgs, inputs, ...}:

{
  programs.zsh.enable = true;

  users.users.v.isNormalUser = true;
  users.users.v.shell = pkgs.zsh;
  users.users.v.initialHashedPassword = "$y$j9T$DHb9ogbZ1eljDvMRXgMNw/$Mof2g9LaIN81XeRPzZEjVH7mLDkdL51kb76gUs1Qj.7";
  users.users.v.extraGroups = [ "wheel" "video" "networkmanager" "audio" ];
  users.users.v.packages = with pkgs; [
      inputs.home-manager.packages.${pkgs.system}.default
      firefox
      vscode
      htop
      brave
      telegram-desktop
      inetutils
      obsidian
    ];
}
