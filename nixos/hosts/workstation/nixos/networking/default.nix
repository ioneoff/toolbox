{ config, ... }:

{
  imports = [
    ./firewall
    ./wireshark
  ];

  networking.hostName = "work";
  networking.networkmanager.enable = true;
}
