{ config, pkgs, ... }:

{

nixpkgs.config.packageOverrides = pkgs: {
  vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
};

hardware.opengl.driSupport32Bit = true;
hardware.opengl.extraPackages = with pkgs; [
  intel-media-driver
  intel-compute-runtime
  vaapiIntel
  vaapiVdpau
  libvdpau-va-gl
];

}
