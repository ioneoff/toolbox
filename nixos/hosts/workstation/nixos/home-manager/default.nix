{ config, inputs, outputs, ... }:

{
  home-manager.extraSpecialArgs = { inherit inputs outputs; };
  home-manager.users.v = import ../../home/home.nix;
}
