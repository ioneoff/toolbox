{ config, pkgs, ... }:

{
  virtualisation.docker.enable = true;
  virtualisation.docker.enableOnBoot = true;
  virtualisation.docker.daemon.settings = 
  {
    bip = "192.168.90.1/24";
  };

  users.users.v.extraGroups = [ "docker" ];
}
